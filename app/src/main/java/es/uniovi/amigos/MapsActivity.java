package es.uniovi.amigos;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private long UPDATE_PERIOD = 1000;
    private GoogleMap mMap;
    private List<Amigo> amigos = new ArrayList<Amigo>();
    String url = "http://192.168.0.16/api/amigos";
    String mUserName = null;
    public int id = -1;
    private String userToken = null;
    BroadcastReceiver mHandlerForBroadcast = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Preguntamos el nombre del usuario
        askUserName();
        //Obtenemos el token
        getFirebaseToken();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        /*Timer timer = new Timer();
        TimerTask updateAmigos = new UpdateAmigosPosition();
        timer.scheduleAtFixedRate(updateAmigos, 0, UPDATE_PERIOD);*/
        //Solicitar permisos
        ActivityCompat.requestPermissions(this,new String[]{
                android.Manifest.permission.ACCESS_FINE_LOCATION
        }, 1);

        actualizaPosicionesNotificacion();

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        getAmigosList();
    }

    public void getAmigosList() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response){
                        System.out.println("Volley OK: " + response);
                        try {
                            mMap.clear();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject objeto_aux = response.getJSONObject(i);
                                String nombre = objeto_aux.getString("name");
                                double latitud = objeto_aux.getDouble("lati");
                                double longitud = objeto_aux.getDouble("longi");
                                Amigo a = new Amigo(nombre, latitud, longitud);
                                amigos.add(a);
                                paintAmigosList(nombre, latitud, longitud);
                            }
                        } catch(JSONException e){}
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError e){
                        System.out.println("Volley: ERROR: " + e);
                    }
                }
        );
        queue.add(request);
    }

    public void paintAmigosList(String nombre, double lat, double longi){
        Marker mark = mMap.addMarker(
                new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .title(nombre)
                        .visible(true));
    }

    class UpdateAmigosPosition extends TimerTask {
        public void run() {
            getAmigosList();
        }
    }

    public void askUserName() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Seleccione el nombre de su amigo");
        alert.setMessage("Nombre:");

        // Crear un EditText para obtener el nombre
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                mUserName = input.getText().toString();
                System.out.println("USUARIO: " + mUserName);
                getAmigoId();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                // Canceled.
            }
        });

        alert.show();
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Tenemos permiso
                    // llamamos a una función que creará las
                    // peticiones de localización
                    SetupLocation();
                } else {
                    // No tenemos permiso... no hacemos nada especial
                    // en esta aplicación para ese caso
                }
                return;
            }
            // La función podría usarse para recibir más permisos, pero
            // no es el caso en esta aplicación
        }
    }

    void SetupLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            // Verificar por si acaso si tenemos el permiso, y si no
            // no hacemos nada
            System.out.println("No tienes los permisos necesarios");
            return;
        }


        // Se debe adquirir una referencia al Location Manager del sistema
        LocationManager locationManager =
                (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Se obtiene el mejor provider de posición
        Criteria criteria = new Criteria();
        String  provider = locationManager.getBestProvider(criteria, false);

        // Se crea un listener de la clase que se va a definir luego
        MyLocationListener locationListener = new MyLocationListener();

        // Se registra el listener con el Location Manager para recibir actualizaciones
        // En este caso pedimos que nos notifique la nueva localización
        // si el teléfono se ha movido más de 10 metros
        locationManager.requestLocationUpdates(provider, 0, 10, locationListener);

        // Comprobar si se puede obtener la posición ahora mismo
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            // La posición actual es location
            System.out.println("Posición actual: "+location);
        } else {
            // Actualmente no se puede obtener la posición
            System.out.println("No es posible obtener la posición.");
        }
    }

    public void nuevaPosicionUsuario(double latitud, double longitud) throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        if(id != -1) {
            String url_put = "http://192.168.0.16:80/api/amigo/"+id;
            JSONObject jsonToSend = new JSONObject();
            jsonToSend.put("id", id);
            jsonToSend.put("lati", latitud);
            jsonToSend.put("longi", longitud);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url_put, jsonToSend,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            System.out.println("Volley OK: " + response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            System.out.println("Volley: ERROR: " + e);
                        }
                    });
            queue.add(request);
        }
        else System.out.println("ID incorrecto");
    }


    public void getAmigoId() {
        String urlAmigo = "http://192.168.0.16:80/api/amigo/byName/" + this.mUserName  ;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,urlAmigo,null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response){
                        try {
                            id= response.getInt("id");
                            System.out.println("Id del usuario seleccionado: " + id);
                            updateDeviceToken(id);

                        } catch(JSONException e){}
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError e){
                        System.out.println("Volley: ERROR: " + e);
                    }
                }
        );
        queue.add(request);

    }

    public void updateDeviceToken(int id) {
        if(id==-1){
            System.out.println("ID -1, no voy a hacer nada");
        }else{
            if(userToken==null){

            }else{
                //Creamos la URL correspondiente
                String urlToken = "http://192.168.0.16:80/api/amigo/" + id;
                RequestQueue queue = Volley.newRequestQueue(this);
                //PUT /api/amigo/{id}
                try{
                    JSONObject jsonToSend = new JSONObject();
                    jsonToSend.put("device", userToken);
                    JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.PUT ,  urlToken,  jsonToSend, null,null);
                    queue.add(objectRequest);
                    System.out.println("El Token ha sido añadido al usuario correspondiente.");
                }
                catch(org.json.JSONException e){
                    System.out.println("Esto ha sido un error... Quizás sea por esto: "+ e);
                }
            }
        }
    }

    void getFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MapsActivity.this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String mToken = instanceIdResult.getToken();
                        System.out.println("FCM: Token -------> " +  mToken);
                        userToken = mToken; //Asigna el valor del token a una variable local
                    }
                });
    }


    void actualizaPosicionesNotificacion(){
         mHandlerForBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                System.out.println("Broadcast: received = " + intent.getAction());
                getAmigosList();
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mHandlerForBroadcast,
                new IntentFilter("updateFromServer"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mHandlerForBroadcast,
                new IntentFilter("updateFromServer"));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mHandlerForBroadcast);
        super.onPause();
    }

    // Se define un Listener para escuchar por cambios en la posición
    class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            // Se llama cuando hay una nueva posición para ese location provider
            double lati = location.getLatitude();
            double longi = location.getLongitude();
            System.out.println("Nueva localización GPS");
            System.out.println(lati + " - " + longi);
            try {
                nuevaPosicionUsuario(lati, longi);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Se llama cuando cambia el estado
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        // Se llama cuando se activa el provider
        @Override
        public void onProviderEnabled(String provider) {}

        // Se llama cuando se desactiva el provider
        @Override
        public void onProviderDisabled(String provider) {}

    }
}