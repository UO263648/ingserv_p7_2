package es.uniovi.amigos;

public class Amigo {

    private String nombre;
    private double latitud;
    private double longitud;

    public Amigo(String name, double lat, double lon){
        this.nombre = name;
        this.latitud = lat;
        this.longitud = lon;
    }
}
